<?php
/**
 * OpenSearch Catalan language file
 */

$catalan = array( 
	'opensearch:title'  =>  "Cerca: %s" , 
	'opensearch:description'  =>  "Resultats de la cerca \"%s\"",
	'opensearch:engine' => 'Motor de cerca %s',
	'opensearh:settings:shortname' => 'Nom curt',
	'opensearh:settings:desc' => 'Descripció',
	'opensearh:settings:longname' => 'Nom llarg',
	'opensearh:settings:icon' => 'Icona',
	'opensearh:settings:tags' => 'Etiquetes',
	'opensearh:settings:lang' => 'Idioma',
	'opensearh:settings:query' => 'Pregunta',
	'opensearh:settings:shortname:description' => 'Conté un petit títol llegible que identifica el motor de cerca. 16 caràcters o menys de text pla. <strong>Requerit</strong>',
	'opensearh:settings:desc:description' => 'Conté una petita descripció llegible sobre el motor de cerca. 1024 caràcters o menys de text pla. <strong>Requerit</strong>',
	'opensearh:settings:longname:description' => 'Conté un títol llegible llarg que identifica el motor de cerca. 48 caràcters o menys de text pla.',
	'opensearh:settings:icon:description' => 'Conté una adreça que identifica la localització d\'una imatge que pot ser utilitzada en associació amb el contingut d\'aquesta cerca. Pot ser un .ico de 16x16, un png de 64x64 o ambdues. Els clients escolliran la imatge que millor encaixi amb l\'espai disponible. El valor ha d\'estar relacionat amb el teu root de l\'Elgg',
	'opensearh:settings:tags:description' => 'Conté un grup de paraules que poden ser utilitzades com a paraules clau per a identificar i categoritzar el contingut d\'aquesta cerca. Les etiquetes han de ser una paraula simple i han de ser delimitades amb el caràcter d\'espai (\' \'). 256 caràcters o menys, l\'espai delimita les etiquetes',
	'opensearh:settings:lang:description' => 'Conté una cadena que indica que el motor de cerca està cercant resultats en un idioma específic. * o codis d\'acord amb la identificació d\'idiomes de l\'XML 1.0',
	'opensearh:settings:query:description' => 'Pregunta de prova disponible pels clients. El clients poden enviar aquesta com una pregunta de prova per assegurar-se que la interfície de l\'OpenSearch funciona.',
); 

add_translation('ca', $catalan); 
